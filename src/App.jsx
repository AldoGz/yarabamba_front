import { useState } from 'react'
import logo from './logo.svg'
import './App.css'


import { MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardImage, MDBBtn } from 'mdb-react-ui-kit';


function App() {
  const [count, setCount] = useState(0);


  const handlCount = () => {
    setCount( count + 1);
  }

  return (
    <div className="App">
      <MDBCard style={{ maxWidth: '22rem' }}>
        <MDBCardImage src='https://media2.giphy.com/media/3o6wrvdHFbwBrUFenu/480w_s.jpg' position='top' alt='...' />
        <MDBCardBody>
          <MDBCardTitle>{count}</MDBCardTitle>
          <MDBCardText>
            ESTE MI EJEMPLO
          </MDBCardText>
          <MDBBtn onClick={handlCount}>+1</MDBBtn>
        </MDBCardBody>
      </MDBCard>
    </div>
  )
}

export default App
